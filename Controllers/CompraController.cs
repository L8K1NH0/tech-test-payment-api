using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CompraController : ControllerBase
    {
        private readonly CompraContext _context;

        public CompraController(CompraContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Create(Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
                return NotFound();

            return Ok(venda);
        }

        [HttpGet("ObterPorNomeProduto")]
        public IActionResult ObterPorNome(string nome)
        {
            var venda = _context.Vendas.Where(x => x.Produto.Contains(nome));
            return Ok(venda);
        }

        [HttpGet("")]
        public IActionResult Obter()
        {
            var venda = _context.Vendas;
            return Ok(venda);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            vendaBanco.Nome = venda.Nome;
            vendaBanco.Telefone = venda.Telefone;
            vendaBanco.Email = venda.Email;
            vendaBanco.CPF = venda.CPF;
            vendaBanco.Produto = venda.Produto;


            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            _context.Vendas.Remove(vendaBanco);
            _context.SaveChanges();
            return NoContent();
        }
    }
}