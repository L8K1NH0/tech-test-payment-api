using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public int CPF { get; set; }
        public int Telefone { get; set; }  
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Produto {get; set;}
    }
}